#!/bin/bash

# On develop branch, fail if there are cs errors and suggest no fix
exitOnBranch() {
  BRANCH=$1
  EXITCODE=${2:-0}
  if [[ "${CI_COMMIT_REF_NAME}" == "${BRANCH}" ]]; then
    exit "${EXITCODE}"
  fi
}

# The Bot ssh key is stored in the gitlab CI Variables
prepareSsh() {
  mkdir -p ~/.ssh
  echo "$SSH_PRIVATE_KEY" >~/.ssh/id_rsa
  chmod 600 ~/.ssh/id_rsa
  echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >~/.ssh/config
}

gitConfig() {
  git config --global user.email 'satis@mehrkanal.com'
  git config --global user.name 'Satis Bot'
}

exitOnNoChanges() {
  EXITCODE=${1:-0}
  if [[ $(git diff --name-only) == "" ]]; then
    exit "${EXITCODE}"
  fi
}

createMergeRequest() {
  BOT_BRANCH=$1
  TITLE=$2
  MERGE_WHEN_PIPELINE_SUCCEEDS=$3
  [[ $CI_PROJECT_URL =~ ^https?://[^/]+ ]] && HOST="${BASH_REMATCH[0]}/api/v4/projects/"
  BODY="{
    \"id\": ${CI_PROJECT_ID},
    \"source_branch\": \"${BOT_BRANCH}\",
    \"target_branch\": \"${CI_COMMIT_REF_NAME}\",
    \"remove_source_branch\": true,
    \"title\": \"${TITLE}\",
    \"assignee_id\":\"${GITLAB_USER_ID}\",
    \"author_username\":\"BOT\",
    \"merge_when_pipeline_succeeds\": \"${MERGE_WHEN_PIPELINE_SUCCEEDS}\"
  }"
  echo "$BODY"
  curl -X POST "${HOST}${CI_PROJECT_ID}/merge_requests" \
    --header "PRIVATE-TOKEN:${TOKEN}" \
    --header "Content-Type: application/json" \
    --data "${BODY}"
  echo -e "${TXT_NOTICE}Opened a new merge request: ${TITLE} and assigned to you ${TXT_CLEAR}"
}

createBranch() {
  BOT_BRANCH=$1
  git checkout -b ${BOT_BRANCH}
}

cleanupBranchIfExists() {
  BOT_BRANCH=$1
  git push -d origin ${BOT_BRANCH} || true
  git branch -D ${BOT_BRANCH} || true
}
