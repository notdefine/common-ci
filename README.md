# Centralize BOT Scripts for automatic code refactoring

Detect and fix CodeStyle and use Instant Upgrades and Automated Refactoring in GitLab Pipelines, based
on [ECS - The Easiest Way to Use Any Coding Standard](https://github.com/symplify/easy-coding-standard) and
[Rector - Instant Upgrades and Automated Refactoring](https://github.com/rectorphp/rector).

If an error is found, the BOT automatically creates a Merge Request for this problem with, which contains the solution.
This will speed up Code Review and reduce frustration.

To use this repository and get automatic MergeRequest Bug fixes with code suggestions, you have to install and
configure [ecs](https://github.com/symplify/easy-coding-standard)
and [rector](https://github.com/rectorphp/rector) first.

This procedure was presented on the [PHPRuhr 2021](https://php.ruhr/)

## Usage

To describe what you need to change to your `gitlab-ci.yml` pipeline configuration, here is an example with and without
the BOT.

Notice you have to set up your Pipeline using [workflow](https://docs.gitlab.com/ee/ci/yaml/#workflow) Settings instead
of except/only first.

### Example gitlab-ci.yml without BOT

```yaml
stages:
  - dependencies
  - tests

Install Composer dependencies:
  stage: dependencies
  image: php
  script:
    - composer install --prefer-dist --no-progress
  artifacts:
    expire_in: 3 days
    paths:
      - vendor/
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: on_success
    - when: never

Tests:
  needs: [ "Install Composer dependencies" ]
  dependencies: [ "Install Composer dependencies" ]
  stage: tests
  image: php
  script:
    - phpunit ...
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: on_success
    - when: never
```

### Example gitlab-ci.yml with BOT included

- To use this Bot, add `bot-checks`, the included yaml file, and the BOT_* variables to your existing GitLab project.
- Create a **BOT User** in your GitLab server, and a SSH key for him.
- Be sure your `composer install` Stage in your PHP project is named `Install Composer dependencies`.
- Add the `SSH_PRIVATE_KEY` Variable in your PHP project CI/CD Settings with the **BOT User** private SSH Key to commit
  to the project.
- Add the `TOKEN` Variable in your PHP project CI/CD Settings with the **BOT User** GitLab API Token, to allow opening
  Merge Requests via Curl API-Request.
- The **BOT User** must be a developer of the project if he should be allowed to push code changes.

```yaml
include:
  remote: https://gitlab.com/notdefine/common-ci/-/raw/main/.gitlab-ci-bot-checks.yml

variables:
  BOT_BRANCHES_EXTENSION: -BOT
  BOT_DOCKER_IMAGE: yourimage/docker/phpx/cli:8.0-build
  BOT_MERGE_WHEN_PIPELINE_SUCCEEDS: "true"
  BOT_RUNNER_TAG: "dind"

stages:
  - dependencies
  - bot-checks
  - tests

Install Composer dependencies:
  stage: dependencies
  image: php
  script:
    - composer install --prefer-dist --no-progress
  artifacts:
    expire_in: 3 days
    paths:
      - vendor/
  rules:
    - if: $CI_COMMIT_REF_NAME =~ /-BOT$/
      when: never
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: on_success
    - when: never

Tests:
  needs: [ "Install Composer dependencies" ]
  dependencies: [ "Install Composer dependencies" ]
  stage: tests
  image: php
  script:
    - phpunit ...
  rules:
    - if: $CI_COMMIT_REF_NAME =~ /-BOT$/
      when: never
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: on_success
    - when: never
```

### Add ECS

```shell
composer require --dev symplify/easy-coding-standard
```

Add a `ecs.php` for code refactoring.

```php
<?php

declare(strict_types=1);

use Symplify\EasyCodingStandard\Config\ECSConfig;use Symplify\EasyCodingStandard\ValueObject\Set\SetList;

return static function (ECSConfig $ECSConfig): void {
    $ECSConfig->paths([
        __DIR__ . '/',
    ]);

    $ECSConfig->sets(
        [
            SetList::COMMON,
            SetList::PSR_12,
            SetList::CLEAN_CODE,
            SetList::SYMPLIFY
        ]
    );
    $ECSConfig->lineEnding("\n");
};
```

### Add Rector

```shell
composer require --dev rector/rector
```

Add a `rector.php` for PSR12 usage.

```php
<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use Symplify\EasyCodingStandard\ValueObject\Set\SetList;

return static function (RectorConfig $rectorConfig): void {
    $rectorConfig->paths([
        __DIR__ . '/src',
        __DIR__ . '/test',
        __DIR__ . '/config',
    ]);

    // import SetList here on purpose to avoid overridden by existing Skip Option in current config
    $rectorConfig->import(SetList::PSR_12);
    $rectorConfig->import(SetList::SYMPLIFY);
    $rectorConfig->import(SetList::COMMON);
    $rectorConfig->import(SetList::CLEAN_CODE);
};
```

## Function

### When a Pipline detects Style or Structure problems

![Pipeline](pipeline.png)

### It creates a new Mergerequest with the automatic created fixes

![Pipeline](new-mr.png)

### So you can take them over.

![Pipeline](changes.png)
![Pipeline](changes2.png)

## Sources

Based on tools

* [Easy Coding Standards](https://github.com/symplify/easy-coding-standard)
* [Rector](https://github.com/rectorphp/rector) *
  *[Buy the Book](https://leanpub.com/rector-the-power-of-automated-refactoring)**

Based on ideas from

* https://gitlab.com/tmaier/gitlab-auto-merge-request/-/blob/develop/merge-request.sh
* https://threedots.tech/post/keeping-common-scripts-in-gitlab-ci/
* https://tomasvotruba.com/blog/2019/06/24/do-you-use-php-codesniffer-and-php-cs-fixer-phpstorm-plugin-you-are-slow-and-expensive/

## Author

Thomas Eimers (eimers@mehrkanal.com)
